* Steps to run the BERT Model:

1. Pull the model code from github repository using git:

$git clone https://github.com/SkullFang/BERT_NLP_Classification.git

2. The pretraining BERT-Base uncased model is used which can be downloaded 
from the following link:

https://storage.googleapis.com/bert_models/2018_10_18/uncased_L-12_H-768_A-12.zip

3. Create BERT_BASE_DIR directory in the porject directory as:

$mkdir BERT_BASE_DIR

4. Unzip the BERT-Base-uncased downloaded model and place all the files in 
the BERT_BASE_DIR directory.

5. Create DATA_DIR directory in the porject directory as:

$mkdir DATA_DIR

6. Place the train.tsv, dev.tsv and test.tsv files in the DATA_DIR directory

7. Place the run.sh script in the directory containing the project and run 
the run.sh script using following command:

$sudo sh run.sh

The model will start training using the train.tsv file, validate using 
the dev.tsv file and will predict the samples that are given in test.tsv file 

* To run the notebook, upload the TRAINING.ipynb file in Google Colab or similar platform,
upload all the files to google drive, mount google drive to it and run the notebook. 
