Steps to generate Training data:

1.Place all the .md files in a specific directory and according to the 
respective skills.

Directory structure:
root_directory-->data-->skill_directory-->.md files

2. Place the generate_chatito_from_md.py file in the root folder. It will 
generate the chatito files in the current directory.

3. To convert chatito files to json files, chatito npm package needs to be installed using:

$npm i chatito --save

4. Now create  the json files from chatito, one at a time, using the following command:

$npx chatito trainClimateBot.chatito 

5. The json file for each skill will be seperately created. Now run the 
generate_csv_from_json.py to generate training dataset in csv format.
