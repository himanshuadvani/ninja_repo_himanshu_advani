import json
import os
import pandas as pd

def main():

	files=list()
	labels=list()
	temp=list()
	dirpath = os.getcwd()
	for foldername, subfoldername, filename in os.walk(dirpath):
		for file in filename:
			if(not 'testing' in file and '.json' in file):				
				files.append(file)
	print(files)

	for file_name in files:
		labels.append(file_name[:-5])

	dic=dict()

	dic["label"]=[]
	dic["text"]=[]

	for file_name in files:
		count=0
		with open(file_name) as json_file:
			data = json.load(json_file)
			file_name=file_name[:-5]
			for p in data['rasa_nlu_data']['common_examples']:
				dic["label"].append(file_name)
				dic["text"].append(p['text'])
			
											

	df = pd.DataFrame(dic)
	df = df.sample(frac=1).reset_index(drop=True)
	train=df[:10000]
	test=df[10001:14000]
	val=df[14000:]		
	train.to_csv("train.tsv",sep="\t")
	test.to_csv("test.tsv",sep="\t")
	val.to_csv("dev.tsv",sep="\t")

if __name__=="__main__":
	main()

