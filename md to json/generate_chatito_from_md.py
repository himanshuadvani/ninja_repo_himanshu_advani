import os
import rstr

def regex_val(data):
    data=data[2:]
    if(data.find("/")==0):
        data=data[1:]
    if(data.find("/i")==(len(data)-2)):
        data=data[:-2]
    return data

def main():
    dirpath = os.getcwd()
    for fn in os.listdir(dirpath):
        if os.path.isdir(fn):
            dir = os.path.abspath(fn)
            #print(dir)

            for fl in os.listdir(dir):
                dir1 = os.path.join(dir, fl)
                #print(dir1)
                files = list()

                for file in os.listdir(dir1):
                    files.append(fn + "/" + fl + "/" + file)

                #print(files)
                dic_ent = {}
                dic_intent = {}
                for file_name in files:
                    final = ""

                    with open(file_name, "r") as f:
                        # f=open(json_file, "r")
                        temp = f.read()
                        dataset = temp.split('\n')
                        reg_flag=False
                        look_flag=False

                        for data in dataset:
                            if data.find("## intent") == 0:
                                reg_flag=False
                                look_flag=False
                                cur_intent = data[len("## intent:"):]
                                if not (data[len("## intent:"):] in dic_intent.keys()):
                                    dic_intent[data[len("## intent:"):]] = set()
                                    continue
                            elif data.find("## regex") == 0:
                                cur_ent=data[len("## regex:"):]
                                reg_flag=True
                                look_flag=False
                                if not (data[len("## regex:"):] in dic_ent.keys()):
                                    dic_ent[data[len("## intent:"):]] = set()
                                    continue
                            elif data.find("## lookup") == 0:
                                cur_ent = data[len("## lookup:"):]
                                reg_flag = False
                                look_flag = True
                                if not (data[len("## regex:"):] in dic_ent.keys()):
                                    dic_ent[data[len("## intent:"):]] = set()
                                    continue
                            else:
                                if(reg_flag==True and look_flag==False and data!=""):
                                    data = regex_val(data)
                                    if cur_ent not in dic_ent.keys():
                                            dic_ent[cur_ent]=set()

                                    for i in range(50):
                                        dic_ent[cur_ent].add(rstr.xeger(data))
                                    #print("Current entity: ", cur_ent)
                                    #print("Data: ",dic_ent[cur_ent])
                                    continue
                                elif (reg_flag == False and look_flag == True and data!=""):
                                    if('.txt' in data):
                                        fd=open(data[5:],'r')
                                        datafromfile=fd.read()
                                        datafromfile = datafromfile.split('\n')
                                        if cur_ent not in dic_ent.keys():
                                            dic_ent[cur_ent]=set()

                                        for key in datafromfile:
                                            dic_ent[cur_ent].add(key)
                                        fd.close()

                                    else:
                                        data=data[2:]
                                        if cur_ent not in dic_ent.keys():
                                            dic_ent[cur_ent] = set()
                                        dic_ent[cur_ent].add(data)

                                    continue
                                else:
                                    sent=data
                                    flag = False
                                    if sent.find('[') == -1:
                                        dic_intent[cur_intent].add(sent[2:])
                                        continue
                                    sent = sent[2:]
                                    temp = sent[:sent.find('[')]
                                    while sent.find('[') != -1:
                                        ent_val = sent[sent.find('[') + 1:sent.find(']')]
                                        temp += '@['
                                        sent = sent[sent.find(']') + 1:]
                                        ent = sent[sent.find('(') + 1:sent.find(')')]
                                        sent = sent[sent.find(')') + 1:]
                                        temp += ent + "]"
                                        if ent not in dic_ent.keys():
                                            dic_ent[ent] = set()
                                        dic_ent[ent].add(ent_val)
                                        if not (sent.find('[') == -1):
                                            temp += sent[:sent.find('[')]
                                    if flag:
                                        dic_intent[cur_intent].add(sent)
                                        continue
                                    else:
                                        dic_intent[cur_intent].add(temp+sent)
                                        continue

                #print("Final dictionary: ", dic_ent)

                if not(os.path.exists("chatito_output/")):
                    os.mkdir("chatito_output/")



                with open("chatito_output/"+fl+".chatito", 'w') as out_file:
                    for k, v in dic_intent.items():
                        out_file.write("%[" + k + "]('training':'1000')")
                        out_file.write("\n")
                        for kv in list(v):
                            if len(kv) > 0:
                                out_file.write("    " + kv)
                                out_file.write("\n")
                        out_file.write("\n")
                    for k, v in dic_ent.items():
                        out_file.write("@[" + k + "]")
                        out_file.write("\n")
                        for kv in list(v):
                            if len(kv) > 0:
                                out_file.write("    " + kv)
                                out_file.write("\n")
                        out_file.write("\n")


if __name__ == "__main__":
    main()
