import json
import os
import pandas as pd

def main():

	files=list()
	labels=list()
	temp=list()
	dirpath = os.getcwd()
	for foldername, subfoldername, filename in os.walk(dirpath):
		for file in filename:
			if('.json' in file):			
					files.append(file)
	print(files)

	for file_name in files:
		labels.append(file_name[:-5])

	dic=dict()
	dic['comment_text']=[]
	for label in labels:
		dic[label]=[]

	for file_name in files:
		with open(file_name) as json_file:
			data = json.load(json_file)
			for p in data['rasa_nlu_data']['common_examples']:
				dic["comment_text"].append(p['text'])
				for label in labels:
					if(p['intent']==label):
						dic[label].append(1)
					else:
						dic[label].append(0)
											

	df = pd.DataFrame(dic)
	print(df)
	df.to_csv("training_data.csv")


if __name__=="__main__":
	main()

